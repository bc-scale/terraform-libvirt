variable "name" {
  type        = string
  description = "Name of the network" 
  default     = "template_network"
}

variable "mode" {
  type        = string
  description = "Network mode"
  default     = "nat"
}

variable "addresses" {
  type        = list(string) 
  description = "Addresses fo the network with prefix length, comma separated"
}

variable "domain" {
  type        = string
  description = "Domain name" 
  default     = "homelab.local"
}

variable "autostart" {
  type        = bool
  description = "Boolean if autostart or not"
  default     = "true"
}
